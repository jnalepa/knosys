# Evolving data-adaptive support vector machines

W. Dudzik, J. Nalepa, M. Kawulok

(Paper submitted to Knowledge-based Systems)

This repository contains:

- The 2D (synthetically generated) datasets (binary classification)
- Our multi-fold splits of the UCI benchmark datasets used for experiments


